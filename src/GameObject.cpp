#include "GameObject.h"

GameObject::GameObject(float x, float z, float speed)
{
    this->x=x;
    this->z=z;
    this->speed=speed;
}

GameObject::~GameObject()
{
    //dtor
}
void GameObject::setDirection(char dir){
    this->direction=dir;
}
char GameObject::getDirection(){
    return this->direction;
}
void GameObject::step(){
    switch (this->direction){
        case 'u':{
            this->x+=this->speed;
            if (this->x > 1.260f){
                this->x = 1.260f;
                // zmiana kierunku

            }
            break;
            }
        case 'd':{
            this->x-=this->speed;
            if (this->x < -1.5099f){
                this->x = -1.5099f;
            }

            break;
            }
        case 'l':{
            this->z-=this->speed;
            if (this->z < -1.190143f){
                this->z=-1.190143f;
            }
            break;}
        case 'r':{
            this->z+=this->speed;
            if (this->z > 1.569){
                this->z = 1.569;
            }
            break;}
    }
}
float GameObject::getX()
{
    return this->x;
}

float GameObject::getZ()
{
    return this->z;
}


float GameObject::getSpeed()
{
    return this->speed;
}


void GameObject::setX(float x) {
    this->x = x;
}

void GameObject::setZ(float z) {
    this->z = z;
}

void GameObject::setSpeed(float speed) {
    this->speed = speed;
}

