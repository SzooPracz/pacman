#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H


class GameObject
{
    public:
        GameObject(float x, float z, float speed);
        virtual ~GameObject();

        float getX();
        float getZ();
        float getSpeed();
        char direction;
        void setDirection(char dir);
        char getDirection();
        void setX(float x);
        void setZ(float z);
        void setSpeed(float z);
        void step();
    protected:

    private:
        float x,z, speed;

};

#endif // GAMEOBJECT_H
