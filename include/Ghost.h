#ifndef GHOST_H
#define GHOST_H


class Ghost
{
    public:
        Ghost();
        virtual ~Ghost();

        char direction;
        float x,y;

    private:
};

#endif // GHOST_H
