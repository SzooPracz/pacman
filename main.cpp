/*
Niniejszy program jest wolnym oprogramowaniem; mo¿esz go
rozprowadzaæ dalej i / lub modyfikowaæ na warunkach Powszechnej
Licencji Publicznej GNU, wydanej przez Fundacjê Wolnego
Oprogramowania - wed³ug wersji 2 tej Licencji lub(wed³ug twojego
wyboru) którejœ z póŸniejszych wersji.

Niniejszy program rozpowszechniany jest z nadziej¹, i¿ bêdzie on
u¿yteczny - jednak BEZ JAKIEJKOLWIEK GWARANCJI, nawet domyœlnej
gwarancji PRZYDATNOŒCI HANDLOWEJ albo PRZYDATNOŒCI DO OKREŒLONYCH
ZASTOSOWAÑ.W celu uzyskania bli¿szych informacji siêgnij do
Powszechnej Licencji Publicznej GNU.

Z pewnoœci¹ wraz z niniejszym programem otrzyma³eœ te¿ egzemplarz
Powszechnej Licencji Publicznej GNU(GNU General Public License);
jeœli nie - napisz do Free Software Foundation, Inc., 59 Temple
Place, Fifth Floor, Boston, MA  02110 - 1301  USA
*/

#define GLM_FORCE_RADIANS

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <stdlib.h>
#include <stdio.h>
#include "constants.hpp"
#include "class_template.hpp"
#include <iostream>
#include <lodepng.h>
#include "include/GameObject.h"
#include <cstdlib>
#include <iostream>
using namespace glm;

float width = 0.3967142f; //szerokość mapy
int steps =  40;   //ilos krokow do zrobenia aby przejsc cała mape

char next_direction = 'd';

GLuint tex[3];
float aspect=1;
float camera_speed_x, camera_speed_y, camera_angle_x, camera_y;

GameObject *pacman;
GameObject *ghosts[4];
GameObject *bananas[64];

Template *ghost_obj;
Template *maze_obj;
Template *pacman_obj;
Template *banana_obj;

void initObjects(){
    //inicjalizacja obiektow Template
    ghost_obj = new Template("objects\\ghost2.obj");
    maze_obj = new Template("objects\\maze3.obj");
    pacman_obj = new Template("objects\\pacman.obj");
    banana_obj = new Template("objects\\banana.obj");


    //inicjalizania GameObjectow

    pacman = new GameObject(-0.319757f, 0.379957f, width/steps);

    ghosts[0] = new GameObject(1.26f, -1.190143f, width/steps);
    ghosts[1] = new GameObject(-1.5099f, -1.190143f, width/steps);
    ghosts[2] = new GameObject(-1.5099f, 1.569f, width/steps);
    ghosts[3] = new GameObject(1.26f, 1.569f, width/steps);


    for(int row=0; row<=7; row++ ){
        for(int col=0; col<=7; col++){
            bananas[row*8+col] = new GameObject(1.26f -(float)row*width, -1.190143f +(float)col*width, 1); //speed =1 oznacza, ze banan istnieje
        }
    }

}

void removeObjects(){
    delete ghost_obj;
    delete maze_obj;
    delete pacman_obj;
    delete pacman;
    delete ghosts;
    delete bananas;
}



//Framebuffer size change event processing
void windowResize(GLFWwindow* window, int width, int height) {
	glViewport(0, 0, width, height); //Generate images in this resolution
	aspect=(float)width/(float)height; //Compute aspect ratio of width to height of the window
}

//Procedura obs³ugi b³êdów
void error_callback(int error, const char* description) {
	fputs(description, stderr);
}



//Procedura obsługi klawiatury
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods){
    if (action == GLFW_PRESS) {

        if (key == GLFW_KEY_LEFT) next_direction = 'l';
        if (key == GLFW_KEY_RIGHT) next_direction = 'r';
        if (key == GLFW_KEY_UP) next_direction = 'u';
        if (key == GLFW_KEY_DOWN) next_direction = 'd';

        if (key == GLFW_KEY_W) camera_speed_y += 0.07f;
        if (key == GLFW_KEY_S) camera_speed_y -= 0.07f;


        if (key == GLFW_KEY_A) camera_speed_x -= 0.07f;
        if (key == GLFW_KEY_D) camera_speed_x += 0.07f;

    }

    if (action == GLFW_RELEASE) {

        if (key == GLFW_KEY_W) camera_speed_y = 0;
        if (key == GLFW_KEY_S) camera_speed_y = 0;

        if (key == GLFW_KEY_A) camera_speed_x = 0;
        if (key == GLFW_KEY_D) camera_speed_x = 0;

    }
}


//Procedura inicjuj¹ca
void initOpenGLProgram(GLFWwindow* window) {
	//************Tutaj umieszczaj kod, który nale¿y wykonaæ raz, na pocz¹tku programu************

	glfwSetFramebufferSizeCallback(window, windowResize);//Zarejestruj procedurê obs³ugi zmiany rozmiaru ekranu.

	glfwSetKeyCallback(window, key_callback); //Zarejestruj procedurę obsługi klawiatury

    glClearColor(0,0,0,1); //Ustaw kolor czyszczenia bufora kolorów na czarno
        //glEnable(GL_COLOR_MATERIAL); //W³¹cz ustawianie koloru materia³u przez polecenia glColor

    std::vector<unsigned char> image;   //Alokuj wektor do wczytania obrazka
    unsigned width, height;   //Zmienne do których wczytamy wymiary obrazka
    //Wczytaj obrazek

    unsigned error = lodepng::decode(image, width, height, "img\\grass.png");

    //Import do pamięci karty graficznej
    glGenTextures(2,tex); //Zainicjuj 2 uchwyty
    glBindTexture(GL_TEXTURE_2D, tex[0]); //Uaktywnij uchwyty
        image.clear();

    //Wczytaj obrazek do pamięci KG skojarzonej z uchwytem
    glTexImage2D(GL_TEXTURE_2D, 0, 4, width, height, 0,
    GL_RGBA, GL_UNSIGNED_BYTE, (unsigned char*) image.data());

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);


    error = lodepng::decode(image, width, height, "img\\ghost.png");
    glBindTexture(GL_TEXTURE_2D,tex[1]);
    image.clear();

    glTexImage2D(GL_TEXTURE_2D, 0, 4, width, height, 0,
        GL_RGBA, GL_UNSIGNED_BYTE, (unsigned char*) image.data());

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);


    error = lodepng::decode(image, width, height, "img\\pacman.png");
    glBindTexture(GL_TEXTURE_2D,tex[2]);
    image.clear();

    glTexImage2D(GL_TEXTURE_2D, 0, 4, width, height, 0,
        GL_RGBA, GL_UNSIGNED_BYTE, (unsigned char*) image.data());

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);




    float pos0[] = {2,2,1,0};
    glLightfv(GL_LIGHT0, GL_POSITION, pos0);

    float dir0[] = {-1,-1,0};
    glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, dir0);

    glLightf(GL_LIGHT0, GL_SPOT_CUTOFF, 45);

    //----
    float pos1[] = {-2,2,1,0};
    glLightfv(GL_LIGHT1, GL_POSITION, pos1);

    float dir1[] = {1,-1,0};
    glLightfv(GL_LIGHT1, GL_SPOT_DIRECTION, dir1);

    float dif1[] = {1,1,1,1};
    glLightfv(GL_LIGHT1, GL_DIFFUSE, dif1);

    float spec1[] = {1,1,1,1};
    glLightfv(GL_LIGHT1, GL_SPECULAR, spec1);

    glLightf(GL_LIGHT1, GL_SPOT_CUTOFF, 45);

    //cienie


    float amb[]={0.5,0.5,0.5,1};
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, amb );

    float dif[]={0.5,0.5,0.5,1};
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, dif );

    float spec[]={0.5,0.5,0.5,1};
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, spec );

    glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 50 );




    glEnable(GL_TEXTURE_2D);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHT1);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_NORMALIZE);

}




void drawScene(GLFWwindow* window) {

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Przygotuj macierze rzutowania i widoku dla renderowanego obrazu
	mat4 P=perspective(50.0f*PI/180.0f,1.0f,1.0f, 50.0f); //Wylicz macierz rzutowania


	mat4 V=lookAt( //Wylicz macierz widoku
    vec3(std::sin(camera_angle_x)*5.0f, 3.0 + camera_y,std::cos(camera_angle_x)*5.0f ),
	vec3(0.0f,0.0f,0.0f),
	vec3(0.0f,1.0f,0.0f));

	glMatrixMode(GL_PROJECTION); //W³¹cz tryb modyfikacji macierzy rzutowania
	glLoadMatrixf(value_ptr(P)); //Skopiuj macierz rzutowania
	glMatrixMode(GL_MODELVIEW); //W³¹cz tryb modyfikacji macierzy model-widok. UWAGA! Macierz ta bêdzie ³adowana przed narysowaniem ka¿dego modelu


    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D,tex[0]);
    mat4 M,R,T,S,I,R2;

    //maze_obj
	glColor4f(1,0,1,1);
    I = mat4(1);
    T = translate(I,vec3(0,0,0));
    S = scale(I,vec3(0.3,0.3,0.3));

	M=T*S;

	glLoadMatrixf(value_ptr(V*M)); //Za³aduj macierz model-widok
	maze_obj -> drawSolid();


    //DUCHY
    glBindTexture(GL_TEXTURE_2D,tex[1]);
	for(int i=0; i<=3; i++) {

        glColor4f(0,1,1,1);
        I = mat4(1);
        T = translate(I,vec3(ghosts[i]->getX(),0,ghosts[i]->getZ()));
        S = scale(I,vec3(0.08,0.08,0.08));


        switch(ghosts[i]->getDirection()){
        case 'u':
            T=translate(T, vec3(0.0f, 0.0f, -0.04f));
            R2=rotate(I, 00.0f*PI/180.0f,vec3(0.0f,1.0f,0.0f));
            break;
        case 'd':
            T=translate(T, vec3(0.0f, 0.0f, -0.04f));
            R2=rotate(I, 180.0f*PI/180.0f,vec3(0.0f,1.0f,0.0f));
            break;
        case 'l':
            T=translate(T, vec3(0.03f, 0.0f, 0.0f));
            R2=rotate(I, 90.0f*PI/180.0f,vec3(0.0f,1.0f,0.0f));
            break;
        case 'r':
            R2=rotate(I, 270.0f*PI/180.0f,vec3(0.0f,1.0f,0.0f));
            break;

        }

        M=T*R2*S;

        glLoadMatrixf(value_ptr(V*M));
        ghost_obj -> drawSolid();
	}

	//pacman_obj
    glBindTexture(GL_TEXTURE_2D,tex[2]);
    glColor4f(1,1,0,1);
    I = mat4(1);
    T=translate(I,vec3(pacman->getX(),0,pacman->getZ()));
    S = scale(I,vec3(0.09f,0.09f,0.09f));

	switch(pacman->getDirection()){
    case 'u':
        T=translate(T, vec3(0.0f, 0.0f, -0.03f));
        R2=rotate(I, 270.0f*PI/180.0f,vec3(0.0f,1.0f,0.0f));
        break;
    case 'd':
        T=translate(T, vec3(0.0f, 0.0f, -0.07f));
        R2=rotate(I, 90.0f*PI/180.0f,vec3(0.0f,1.0f,0.0f));
        break;
    case 'l':
        T=translate(T, vec3(0.03f, 0.0f, 0.0f));
        R2=rotate(I, 0.0f*PI/180.0f,vec3(0.0f,1.0f,0.0f));
        break;
    case 'r':
        R2=rotate(I, 180.0f*PI/180.0f,vec3(0.0f,1.0f,0.0f));
        break;

	}
	M=T*R2*S;

	glLoadMatrixf(value_ptr(V*M)); //Za³aduj macierz model-widok
	pacman_obj -> drawSolid();


	//BANANAS
	for(int i=0; i<=63; i++) {

        glColor4f(0,1,1,1);
        I = mat4(1);
        T = translate(I,vec3(bananas[i]->getX(),0,bananas[i]->getZ()));
        S = scale(I,vec3(0.05,0.05,0.05));

        R2=rotate(I, (float)glfwGetTime()*PI*2.0f,vec3(0.0f,1.0f,0.0f));


        M=T*R2*S; // wymnażanie macierzy, od prawej

        glLoadMatrixf(value_ptr(V*M)); //Za³aduj macierz model-widok

        if(bananas[i]->getSpeed() == 1) banana_obj -> drawSolid();
	}



    printf("%f, %f\n",pacman->getX(), pacman->getZ());

	glfwSwapBuffers(window);
}

int main(void)
{
    initObjects();//inicjujemy obiekty

	GLFWwindow* window; //WskaŸnik na obiekt reprezentuj¹cy okno

	glfwSetErrorCallback(error_callback);//Zarejestruj procedurê obs³ugi b³êdów

	if (!glfwInit()) { //Zainicjuj bibliotekê GLFW
		fprintf(stderr, "Nie mo¿na zainicjowaæ GLFW.\n");
		exit(EXIT_FAILURE);
	}

	window = glfwCreateWindow(800, 800, "OpenGL", NULL, NULL);


	if (!window) //Je¿eli okna nie uda³o siê utworzyæ, to zamknij program
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	glfwMakeContextCurrent(window); //Od tego momentu kontekst okna staje siê aktywny i polecenia OpenGL bêd¹ dotyczyæ w³aœnie jego.
	glfwSwapInterval(1); //Czekaj na 1 powrót plamki przed pokazaniem ukrytego bufora

	GLenum err;
	if ((err=glewInit()) != GLEW_OK) { //Zainicjuj bibliotekê GLEW
		fprintf(stderr, "Nie mo¿na zainicjowaæ GLEW: %s\n", glewGetErrorString(err));
		exit(EXIT_FAILURE);
	}

	initOpenGLProgram(window); //Operacje inicjuj¹ce

	glfwSetTime(0); //Wyzeruj licznik czasu

    camera_angle_x = -1.54f;
	int counter = 0;

	//G³ówna pêtla
	bool running = true;
	while (!glfwWindowShouldClose(window))
	{

        //co steps pojawia sie skrzyzowanie i nalezy podjac decyzje w odnosnie kierunku
        if (counter % steps == 0) {
            counter = 0;

            pacman->setDirection(next_direction);

            for(int i=0; i<=3; i++) {

                switch(std::rand() % 4){
                    case 0:
                        ghosts[i]->setDirection('u');
                        break;
                    case 1:
                        ghosts[i]->setDirection('d');
                        break;
                    case 2:
                        ghosts[i]->setDirection('l');
                        break;
                    case 3:
                        ghosts[i]->setDirection('r');
                        break;
                    }
            }
	    }

	    counter += 1;

        camera_angle_x += camera_speed_x;
        camera_y += camera_speed_y;

        pacman->step();
        for(int i=0; i<=3; i++){
            ghosts[i]->step();

            //wykrywanie kolizji z duchami
            if(abs(ghosts[i]->getX() - pacman->getX()) <0.04
                and  abs(ghosts[i]->getZ() - pacman->getZ()) <0.04)
                    std::cout<<"buuuuuuum";
                    running = false;

        }


        // wykrywanie kolizji z bananami
        for(int i=0; i<=63; i++){
            if(abs(bananas[i]->getX() - pacman->getX()) <0.03
                and  abs(bananas[i]->getZ() - pacman->getZ()) <0.03
                and bananas[i]->getSpeed() == 1.0f)
                {
                    bananas[i]->setSpeed(0.0f); //jesli zjedzony to ustaw speed na 1.0
                    std::cout<<" zjadlem ";
                }
        }

		drawScene(window); //Wykonaj procedurę rysującą
		glfwPollEvents(); //Wykonaj procedury callback w zalezności od zdarzeń jakie zaszły.
	}

	//while(!glfwWindowShouldClose(window)){};
    glDeleteTextures(2,tex);
	glfwDestroyWindow(window); //Usuñ kontekst OpenGL i okno
	glfwTerminate(); //Zwolnij zasoby zajête przez GLFW
	exit(EXIT_SUCCESS);

	removeObjects();//usuwamy obiekty
}
